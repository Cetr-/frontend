# One time set-up

1. Install node > v14
2. Install npm/yarn
3. git clone `git clone https://Cetr-@bitbucket.org/Cetr-/frontend.git`
4. Run `cd frontend`
5. Run `npm install .` or `yarn install `
6. Run `yarn start`

# Tests

1. Run `yarn test` or `npm run test`
