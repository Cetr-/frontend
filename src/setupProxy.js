const { createProxyMiddleware } = require("http-proxy-middleware");
module.exports = function (app) {
  app.use(
    ["/v1/api", "/v1/auth/google"],
    createProxyMiddleware({
      target: "http://localhost:3000", // needed only for development as there are different servers for client and server
    })
  );
};