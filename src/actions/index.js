import axios from "axios";
import { FETCH_USER } from "./types";

// Redux Thunk gives a handle to a dispatch Function to send the action to all differrent
// reducers in the store, causing them to instantly recalculate the app state

// This is an action creator
export const fetchUser = () => async (dispatch) => {
    const res = await axios.get("/v1/api/current_user");
    dispatch({type: FETCH_USER, payload: res.data});
}