import { useEffect, useState } from 'react';
import axios from 'axios';

function useFetch(url, resourceName, city, state) {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    setResponse(null);
    setError(null);
    if (!url) return;
    axios
      .get(url, {
        params: { resourceName: resourceName, city: city, state: state },
        headers: { 'Access-Control-Allow-Origin': '*' },
      })
      .then(
        (json) => {
          setResponse(json.data);
        },
        (error) => {
          setError(error);
        }
      );
  }, [url, resourceName, state, city]);

  return { response, error };
}

function usePut(url, resourceId, updatedFields) {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    if (!url) return;
    axios
      .put(`${url}/${resourceId}`, {
        body: { ...updatedFields },
        headers: { 'Access-Control-Allow-Origin': '*' },
      })
      .then(
        (json) => {
          setResponse(json.data);
        },
        (error) => {
          setError(error);
        }
      );
  }, [url, resourceId, updatedFields]);

  return { response, error };
}

export { useFetch, usePut };
