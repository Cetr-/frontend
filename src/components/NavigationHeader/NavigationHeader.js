import React from 'react';
import { connect } from 'react-redux';
import { Navbar, Form, FormControl, Button } from 'react-bootstrap';

const NavigationHeader = (props) => {
  const loginContent = () => {
    if (props.auth) {
      return (
        <Form inline>
          <Button href="/v1/api/create" style={{ margin: '0px 0px 0px 10px' }}>
            Add Resource
          </Button>
          <Button
            href="/v1/api/logout"
            variant="outline-success"
            style={{ margin: '0px 0px 0px 10px' }}
          >
            Logout
          </Button>
        </Form>
      );
    }
    return (
      <li>
        {' '}
        <Button href="/v1/auth/google">Login with Google</Button>{' '}
      </li>
    );
  };

  return (
    <Navbar
      bg="primary"
      variant="dark"
      fixed="top"
      expand="sm"
      className="bg-dark justify-content-between"
    >
      <Navbar.Brand href="#home">COVID-AID</Navbar.Brand>
      {loginContent()}
    </Navbar>
  );
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

export default connect(mapStateToProps)(NavigationHeader);
