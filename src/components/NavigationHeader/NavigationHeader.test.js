import { render, screen } from '@testing-library/react';
import NavigationHeader from './NavigationHeader';

test('renders NavigationHeader as expected', () => {
  render(<NavigationHeader />);
  const title = screen.getByText('COVID-AID');
  expect(title).toBeInTheDocument();
});
