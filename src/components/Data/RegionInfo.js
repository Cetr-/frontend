import csc from "country-state-city";

const states = csc.getStatesOfCountry("IN");

function getCitiesFromState(state_coude) {
    return csc.getCitiesOfState("IN", state_coude);
}

export {getCitiesFromState};
export default states;