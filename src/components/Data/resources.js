const resources = [
    {
      "name": "FoodService"
    },
    {
      "name": "Helpline"
    },
    {
      "name": "Hospital"
    },
    {
      "name": "Medicine"
    },
    {
      "name": "Oxygen"
    },
    {
      "name": "PcrTest"
    },
    {
      "name": "PlasmaDonor"
    },
    {
      "name": "Vaccine"
    }
  ];

  export default resources;