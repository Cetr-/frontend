import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

const SelectionDropdown = (props) => {
  const [value, setValue] = React.useState('');
  const [open, setOpen] = React.useState(false);

  const classes = useStyles();

  const handleChange = (event) => {
    setValue(event.target.value);
    props?.handleCallback?.(
      props.selectionOptions.filter(
        (item) => item.name === event.target.value
      )[0]
    );
  };

  const handleClose = () => {
    setOpen(false);
    props?.handleClose?.();
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const items = props.selectionOptions.map((item) => {
    return <MenuItem value={item.name}>{item.name}</MenuItem>;
  });

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel>{props.fieldName}</InputLabel>
        <Select
          open={open || props.open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={value || props.defaultValue || ''}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {items}
        </Select>
      </FormControl>
    </div>
  );
};

export default SelectionDropdown;
