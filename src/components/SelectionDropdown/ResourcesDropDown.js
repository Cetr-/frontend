import React, { useEffect, useState } from 'react';
import axios from 'axios';
import SelectionDropdown from './SelectionDropdown.js';

const BASE_URL_TYPES = '/web/types';

const ResourcesDropDown = (props) => {
  const { stateName, cityName } = props;
  const [resourceListResponse, setResourceListResponse] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    setResourceListResponse(null);
    setError(null);
    axios
      .get(BASE_URL_TYPES, {
        params: { state: stateName, city: cityName },
        headers: { 'Access-Control-Allow-Origin': '*' },
      })
      .then(
        (json) => {
          setResourceListResponse(json.data);
        },
        (error) => {
          setError(error);
        }
      );
  }, [stateName, cityName]);

  if (!resourceListResponse) {
    return <p>Loading resources ....</p>;
  }

  if (error || !resourceListResponse.types) {
    return <p>Whoops...looks like backend is sleeping, ping developer!</p>;
  }

  return (
    <SelectionDropdown
      selectionOptions={resourceListResponse.types.map((city) => ({
        name: city,
      }))}
      fieldName="Resources"
      handleCallback={props.setResource}
      defaultValue={props.resourceName}
    />
  );
};

export default ResourcesDropDown;
