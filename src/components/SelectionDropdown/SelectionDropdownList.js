import React, { useEffect, useState } from 'react';
import StateDropDown from './StateDropdown';
import CityDropDown from './CityDropDown';
import ResourcesDropDown from './ResourcesDropDown';

const SelectionDropdownWithApis = (props) => {
  const [state, setState] = React.useState('');
  const [city, setCity] = React.useState('');
  const [resource, setResource] = React.useState('');

  useEffect(() => {
    props.setFilters({ state, city: '', resource: '' });
    setCity('');
    setResource('');
  }, [state]);
  useEffect(() => {
    props.setFilters({ city });
  }, [city]);
  useEffect(() => {
    props.setFilters({ resource });
  }, [resource]);

  if (state === '') {
    return <StateDropDown setState={setState} />;
  }

  return (
    <>
      <StateDropDown setState={setState} />
      <CityDropDown stateName={state.name} setCity={setCity} />
      <ResourcesDropDown
        stateName={state.name}
        cityName={city.name}
        resourceName={resource.name}
        setResource={setResource}
      />
    </>
  );
};

export default SelectionDropdownWithApis;
