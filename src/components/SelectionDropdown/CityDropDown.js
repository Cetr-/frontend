import React, { useEffect, useState } from 'react';
import axios from 'axios';
import SelectionDropdown from './SelectionDropdown.js';

const BASE_URL_CITY = '/web/cities';

const CityDropDown = (props) => {
  const { stateName } = props;
  const [cityListResponse, setCityListResponse] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    setCityListResponse(null);
    setError(null);
    axios
      .get(BASE_URL_CITY, {
        params: { state: stateName },
        headers: { 'Access-Control-Allow-Origin': '*' },
      })
      .then(
        (json) => {
          setCityListResponse(json.data);
        },
        (error) => {
          setError(error);
        }
      );
  }, [stateName]);

  if (!cityListResponse) {
    return <p>Loading cities ....</p>;
  }

  if (error || !cityListResponse.cities) {
    return <p>Whoops...looks like backend is sleeping, ping developer!</p>;
  }

  return (
    <SelectionDropdown
      selectionOptions={cityListResponse.cities.map((city) => ({
        name: city,
      }))}
      fieldName="City"
      handleCallback={props.setCity}
    />
  );
};

export default CityDropDown;
