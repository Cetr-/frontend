import React, { useEffect, useState } from 'react';
import axios from 'axios';
import SelectionDropdown from './SelectionDropdown.js';

const BASE_URL_STATE = '/web/states';

const StateDropDown = (props) => {
  const [stateListResponse, setStateListResponse] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    setStateListResponse(null);
    setError(null);
    axios
      .get(BASE_URL_STATE, {
        headers: { 'Access-Control-Allow-Origin': '*' },
      })
      .then(
        (json) => {
          setStateListResponse(json.data);
        },
        (error) => {
          setError(error);
        }
      );
  }, []);

  if (!stateListResponse) {
    return <p>Loading the page....</p>;
  }

  if (error || !stateListResponse.states) {
    return <p>Whoops...looks like backend is sleeping, ping developer!</p>;
  }

  return (
    <SelectionDropdown
      selectionOptions={stateListResponse.states.map((state) => ({
        name: state,
      }))}
      fieldName="State"
      handleCallback={props.setState}
    />
  );
};

export default StateDropDown;
