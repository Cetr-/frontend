import { toast } from 'react-toastify';
import axios from 'axios';

const BASE_RESOURCE_URL = '/web/resources';

export const handleChangeForEdits = async (
  oldValue,
  updatedValue,
  { id, field, api }
) => {
  try {
    api.commitCellChange({ id, field, props: { value: 'Uploading...' } });
    api.setCellMode(id, field, 'view');

    const result = await axios.put(
      `${BASE_RESOURCE_URL}/${id}`,
      { [field]: updatedValue },
      {
        headers: { 'Access-Control-Allow-Origin': '*' },
      }
    );

    if (result.data.resource) {
      const updatedField = result.data.resource[0][field];
      api.commitCellChange({
        id,
        field,
        props: {
          value: updatedField,
        },
      });
      api.setCellMode(id, field, 'view');
      toast.success(result.data.message);
    } else {
      toast.warn(`This field cannot be edited!`);
    }
  } catch (apiError) {
    toast.error(
      `Sorry, couldn't update field, something went wrong: ${apiError}`
    );
    api.commitCellChange({ id, field, props: { value: oldValue } });
    api.setCellMode(id, field, 'view');
  }
};
