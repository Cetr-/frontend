import React from 'react';
import ContactPhoneIcon from '@material-ui/icons/ContactPhone';

export const PhoneNumber = ({ phoneNumber }) => {
  return (
    <>
      <ContactPhoneIcon color="primary" style={{ fontSize: 20 }} />
      <a href={`tel:${phoneNumber}`}> {phoneNumber}</a>
    </>
  );
};
