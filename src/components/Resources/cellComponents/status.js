import React from 'react';
import SelectionDropdown from '../../SelectionDropdown/SelectionDropdown';
import { handleChangeForEdits } from './handleChangeForEdits';

const VerifiedStatusDropdown = ({ value }) => {
  return <p>{value || 'UNVERIFIED'}</p>;
};

const VerifiedStatusDropdownEditable = (props) => {
  const { id, value, api, field } = props;

  const handleChange = React.useCallback((event) => {
    handleChangeForEdits(value, event?.name, { id, field, api });
  }, []);

  const handleClose = () => {
    api.setCellMode(id, field, 'view');
  };

  const statusList = [
    { name: 'UNVERIFIED' },
    { name: 'VERIFIED_AVAILABLE' },
    { name: 'VERIFIED_RECHECK_AVAILABILITY' },
    { name: 'VERIFIED_UNAVAILABLE' },
    { name: 'NOT_REACHABLE' },
    { name: 'FAKE' },
  ];

  return (
    <div>
      <SelectionDropdown
        selectionOptions={statusList}
        fieldName="Status"
        handleCallback={handleChange}
        handleClose={handleClose}
        defaultValue={value || 'UNVERIFIED'}
      />
    </div>
  );
};

export { VerifiedStatusDropdown, VerifiedStatusDropdownEditable };
