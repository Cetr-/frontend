import React from 'react';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownAltIcon from '@material-ui/icons/ThumbDownAlt';
import { IconButton } from '@material-ui/core';
import { handleChangeForEdits } from './handleChangeForEdits';

export const VerificationCount = (props) => {
  const { id, value, api, field, isEditable } = props;

  const handleChange = React.useCallback((votes) => {
    handleChangeForEdits(value, votes, { id, field, api });
  }, []);

  return (
    <>
      {value || 0}
      {isEditable && (
        <>
          <IconButton aria-label="upvote" onClick={() => handleChange(1)}>
            <ThumbUpIcon color="primary" style={{ fontSize: 20 }} />
          </IconButton>
          <IconButton aria-label="upvote" onClick={() => handleChange(-1)}>
            <ThumbDownAltIcon color="secondary" style={{ fontSize: 20 }} />
          </IconButton>
        </>
      )}
    </>
  );
};
