import React, { useState, useEffect } from 'react';
import { useFetch } from '../../utils/customHooks';
import renderCellExpand from './cellComponents/GridCellExpand';
import { DataGrid } from '@material-ui/data-grid';
import { PhoneNumber } from './cellComponents/phoneNumber';
import {
  VerifiedStatusDropdown,
  VerifiedStatusDropdownEditable,
} from './cellComponents/status';
import { connect } from 'react-redux';
import { VerificationCount } from './cellComponents/verifiedCounts';
import axios from 'axios';
import { toast } from 'react-toastify';

const BASE_RESOURCE_URL = '/web/resources';

const ResourceTableGrid = ({ resourceName, city, state, auth }) => {
  const [rows, setRows] = useState([]);

  const handleBlur = React.useCallback((params, event) => {
    if (params.cellMode === 'edit' && params.field === 'STATUS') {
      event.stopPropagation();
    }
  }, []);

  const handleClick = React.useCallback((params, event) => {
    if (
      params.cellMode === 'view' &&
      ['ADDITIONAL_COMMENTS'].includes(params.field)
    ) {
      params.api.setCellMode(params.id, params.field, 'edit');
    }
  }, []);

  const handleEditCellChangeCommitted = React.useCallback(
    async (params) => {
      const { id, field, props } = params;
      const rowToUpdate = rows.find((row) => row.id === id);
      if (!rowToUpdate || (rowToUpdate && rowToUpdate[field] === props.value)) {
        // Don't call API when value doesn't change
        return;
      }
      try {
        const result = await axios.put(
          `${BASE_RESOURCE_URL}/${id}`,
          { [field]: props.value },
          {
            headers: { 'Access-Control-Allow-Origin': '*' },
          }
        );
        if (result.data.resource) {
          const updatedRows = rows.map((row) => {
            if (row.id === id) {
              return { ...row, [field]: props.value };
            }
            return row;
          });
          setRows(updatedRows);
          toast.success('Field successfully updated!');
        } else {
          toast.warn(`This field cannot be edited!`);
        }
      } catch (apiError) {
        toast.error(
          `Sorry, couldn't update field, something went wrong: ${apiError}`
        );
      }
    },
    [rows]
  );

  const { response, error } = useFetch(
    BASE_RESOURCE_URL,
    resourceName,
    city,
    state
  );

  useEffect(() => {
    if (response && response.resources.values) {
      setRows(response.resources.values);
    }
  }, [response]);

  if (error) {
    return <div>Error fetching Resource. Try Again Later.</div>;
  }

  if (!response) {
    return <div>Fetching...</div>;
  }

  const resources = response.resources.values;
  const status = response.status;
  const message = response.resources.message;

  if (status === 404 || !resources?.length) {
    return <div>{message || 'No resource found'}</div>;
  }

  const columns = [
    {
      field: 'SUBTYPE',
      headerName: 'Subtype',
      width: 130,
      renderCell: renderCellExpand,
    },
    {
      field: 'VENDOR_NAME',
      headerName: 'Provider',
      width: 130,
      type: 'string',
      renderCell: renderCellExpand,
    },
    {
      field: 'PHONE_NUMBER',
      headerName: 'Number ',
      width: 150,
      renderCell: (params) => <PhoneNumber phoneNumber={params.value} />,
    },
    {
      field: 'STATUS',
      headerName: 'Status',
      width: 200,
      editable: !!auth,
      renderCell: (params) => <VerifiedStatusDropdown {...params} />,
      renderEditCell: (params) => (
        <VerifiedStatusDropdownEditable {...params} />
      ),
    },
    {
      field: 'VERIFICATION_COUNT',
      headerName: 'Verif. Count',
      width: 150,
      editable: !!auth,
      renderCell: (params) => (
        <VerificationCount isEditable={false} {...params} />
      ),
      renderEditCell: (params) => <VerificationCount isEditable {...params} />,
    },
    {
      field: 'ADDITIONAL_COMMENTS',
      headerName: 'Additional comment',
      type: 'string',
      editable: auth ? true : false,
      width: 200,
      renderCell: renderCellExpand,
    },
    {
      field: 'CHECK_AFTER',
      headerName: 'Check After',
      width: 130,
      renderCell: renderCellExpand,
    },
    {
      field: 'LAST_UPDATED',
      type: 'string',
      headerName: 'Last updated',
      editable: auth ? true : false,
      width: 150,
      renderCell: renderCellExpand,
    },
  ];

  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        autoHeight
        columns={columns}
        pageSize={100}
        onCellBlur={handleBlur}
        onCellClick={handleClick}
        rows={rows}
        onEditCellChangeCommitted={handleEditCellChangeCommitted}
      />
    </div>
  );
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

export default connect(mapStateToProps)(ResourceTableGrid);
