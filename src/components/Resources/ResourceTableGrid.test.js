import { render, screen, waitFor } from '@testing-library/react';
import ResourceTableGrid from './ResourceTableGrid';
import axios from 'axios';

jest.mock('axios');

test('renders Table Grid when response is 200 OK', async () => {
  const stubResponse = {
    data: {
      message: [
        {
          resourceName: 'hospitalBeds',
          address: 'Delhi',
          lastUpdated: '2021-04-18T00:00:00.000Z',
          phoneNumbers: [9999999990],
          availablityCount: 'unavailable',
          additionalComments: 'no comments',
          verified: false,
        },
      ],
    },
  };
  axios.get.mockImplementationOnce(() => Promise.resolve(stubResponse));
  render(<ResourceTableGrid url={'https://dummyapi.com/'} />);
  await waitFor(() => screen.getByText('no comments'));
  expect(screen.getByText('Delhi')).toBeInTheDocument();
});
a;
