import SelectionDropdowns from '../SelectionDropdown/SelectionDropdownList';
import ResourceTableGrid from '../Resources/ResourceTableGrid';
import React from 'react';

const Search = () => {
  const [filters, setFilters] = React.useState({});

  const updateFilters = (newFilters) => {
    // combine old and new filter
    setFilters({ ...filters, ...newFilters });
  };

  const resourceTableGrid = (resource = '', state = '', city = '') => {
    if (resource && state) {
      return (
        <ResourceTableGrid resourceName={resource} state={state} city={city} />
      );
    }
    return <></>;
  };

  return (
    <div>
      <SelectionDropdowns setFilters={updateFilters} />
      {resourceTableGrid(
        filters.resource?.name,
        filters.state?.name,
        filters.city?.name
      )}
    </div>
  );
};

export default Search;
