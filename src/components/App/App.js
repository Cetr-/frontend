import React, { useEffect } from 'react';
import NavigationHeader from '../NavigationHeader/NavigationHeader';
import Search from '../Search/Search';
import { connect } from "react-redux";
import * as actions from '../../actions';

const App = (props) => {
  useEffect(() => {
    async function fetchUser() { 
      await props.fetchUser();
  }
  fetchUser();
});
  
  return (
    <div className="App">
      <NavigationHeader />
      <div className="content">
        <Search />
      </div>
    </div>
  );
};

export default connect(null, actions)(App);
